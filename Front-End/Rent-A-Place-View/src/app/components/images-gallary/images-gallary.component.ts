import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DashboardComponent } from 'src/app/pages/dashboard/dashboard.component';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-images-gallary',
  templateUrl: './images-gallary.component.html',
  styleUrls: ['./images-gallary.component.scss']
})
export class ImagesGallaryComponent implements OnInit {
  images: any[] = [];
  constructor(
    public dialogRef: MatDialogRef<DashboardComponent>,
    private api: ApiService,
    @Inject(MAT_DIALOG_DATA) public data: { propertyId: number }
  ) { }

  ngOnInit(): void {
    this.getImages();
  }

  getImages() {
    this.api.get(`/api/properties/all-images/${this.data.propertyId}`).subscribe((res: any) => {
      this.images = res?.data || [];
    });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
