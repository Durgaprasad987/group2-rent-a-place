import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoookPropertyComponent } from './boook-property.component';

describe('BoookPropertyComponent', () => {
  let component: BoookPropertyComponent;
  let fixture: ComponentFixture<BoookPropertyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoookPropertyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BoookPropertyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
